<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    //default
    // Table name
    // protected $table = 'posts';
    // Primary key
    // public $primaryKey = 'id;'
    // Timesstamps
    // public $timestamps = true;

    public function user(){
      return $this->belongsTo('App\User');
    }
}
