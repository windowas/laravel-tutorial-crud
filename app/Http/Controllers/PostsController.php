<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Post;

class PostsController extends Controller
{

  /**
   * Create a new controller instance.
   *
   * @return void
   */
  public function __construct()
  {
      $this->middleware('auth', ['except' =>['index', 'show']]);
  }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = Post::orderBy('created_at','desc')->paginate(10);
        // $posts = Post::orderBy('created_at','desc')->get();
        // $posts = Post::orderBy('created_at','desc')->take(1)->get();
        return view('posts.index')->with('posts', $posts);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('posts.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'body' => 'required',
            'image' => 'image|nullable|max:1999'
        ]);

        // handle file uopz_overload
        if($request->hasFile('image')){
          // get file name with the extention (no good for same name)
          $fileNameWithExt = $request->file('image')->getClientOriginalName();
          // get just file name
          $filename = pathinfo($fileNameWithExt, PATHINFO_FILENAME);
          // get just extension
          $extension = $request->file('image')->getClientOriginalExtension();
          //Filname to Store
          $fileNameToStore = $filename.'_'.time().'.'.$extension;
          // upload the image
          $path = $request->file('image')->storeAs('public/images', $fileNameToStore);
        } else {
          $fileNameToStore = 'noImage.jpg';
        }

        //create post
        $post = new Post;
        $post->title = $request->input('title');
        $post->body = $request->input('body');
        $post->user_id = auth()->user()->id;
        $post->image = $fileNameToStore;
        $post->save();

        return redirect('/posts')->with('success', 'Post Created');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $post = Post::find($id);
        return view('posts.show')->with('post', $post);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $post = Post::find($id);

        // check for corrent user
        if(auth()->user()->id !== $post->user_id){
          return redirect('/posts')->with('error', 'Unauthorized Action');
        }
        return view('posts.edit')->with('post', $post);
    }

    /**
     * Update the specified resource in storage.;
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title' => 'required',
            'body' => 'required'
        ]);

        // handle file uopz_overload
        if($request->hasFile('image')){
          // get file name with the extention (no good for same name)
          $fileNameWithExt = $request->file('image')->getClientOriginalName();
          // get just file name
          $filename = pathinfo($fileNameWithExt, PATHINFO_FILENAME);
          // get just extension
          $extension = $request->file('image')->getClientOriginalExtension();
          //Filname to Store
          $fileNameToStore= $filename.'_'.time().'.'.$extension;
          // upload the image
          $path = $request->file('image')->storeAs('public/images', $fileNameToStore);
        }

        //create post
        $post = Post::find($id);

        if($post->image !== 'noImage,jpg'){
          // Delete the image
          Storage::delete('public/images/'.$post->image);
        }

        // check for corrent user
        if(auth()->user()->id !== $post->user_id){
          return redirect('/posts')->with('error', 'Unauthorized Action');
        }
        $post->title = $request->input('title');
        $post->body = $request->input('body');
        if($request->hasFile('image')){
          $post->image = $fileNameToStore;
        }
        $post->save();

        // return redirect('posts/' [$post->id])->with('success', 'Post Updated');
        // redirect back to post updated
        return redirect()->action('PostsController@show', ['post' =>$post])->with('success', 'Post Updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post = Post::find($id);

        // check for corrent user
        if(auth()->user()->id !== $post->user_id){
          return redirect('/posts')->with('error', 'Unauthorized Action');
        }
        //only delete if its not no image
        if($post->image !== 'noImage,jpg'){
          // Delete the image
          Storage::delete('public/images/'.$post->image);
        }

        $post->delete();
        return redirect('/posts')->with('success', 'Post Deleted');
    }
}
