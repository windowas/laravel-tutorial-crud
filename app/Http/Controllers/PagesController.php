<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PagesController extends Controller
{
  //php artisan make:controller PagesController
    public function index(){
      return view('pages.index');
    }

    public function contacts(){
      return view('pages.contacts');
    }

    public function page1(){
      return view('pages.page1');
    }
}
