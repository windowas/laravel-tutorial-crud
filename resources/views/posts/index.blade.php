@extends('layouts.app')

@section('content')
    <h1>Posts</h1>

    @if(count($posts) > 0)
        @foreach($posts as $post)
            <a href='posts/{{$post->id}}'>
              <div class="well">
                <div class="row">
                  <div class="col-md-4 col-sm-4">
                    <img src="/storage/images/{{$post->image}}" alt="Image" width='100%'>
                  </div>
                  <div class="col-md-8 col-sm-8">
                    <h3>{{$post->title}}</h3>
                    <small>Written on {{$post->created_at}} by {{$post->user->name}}</small>
                  </div>
                </div>
            </div></a>
        @endforeach
        {{$posts->links()}}
    @else
        <p>No posts found</p>
        @endif
@endsection
