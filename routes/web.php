<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});*/

/*Route::get('/contacts', function () {
    return view('pages.contacts');
});*/

//dynamic parameterinto url
/*Route::get('/users/{id}', function ($id) {
    return 'This returns id of '.$id;
});*/

//controller action
Route::get('/', 'PagesController@index');

Route::get('/contacts', 'PagesController@contacts');

Route::get('/page1', 'PagesController@page1');

Route::resource('posts', 'PostsController');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
